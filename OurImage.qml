import QtQuick 2.5
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import org.kde.kirigami 2.5 as Kirigami

Image {
    Layout.fillWidth: true
    Layout.fillHeight: true
    Layout.margins: 100
    fillMode: Image.PreserveAspectFit
}
