import QtQuick 2.5
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import org.kde.kirigami 2.5 as Kirigami

Kirigami.AbstractApplicationWindow
{
    Loader {
        anchors {
            fill: parent
            margins: 100
        }
        Rectangle {
            color: "black"
            z: -1
            anchors {
                fill: parent
                margins: -100
            }
        }

        Component {
            id: plamo_1
            ColumnLayout {
                OurImage {
                    source: "plasmaphone.svg"
                }
                Text {
                    text: "Plasma Mobile"
                    color: "white"
                    Layout.alignment: Qt.AlignCenter
                    font.pointSize: 40
                }
            }
        }
        Component {
            id: plamo_2
            ColumnLayout {
                OurImage {
                    source: "plasmaphone.svg"
                }
                Text {
                    text: "https://www.plasma-mobile.org/"
                    color: "white"
                    Layout.alignment: Qt.AlignCenter
                    font.pointSize: 40
                }
            }
        }
        Component {
            id: plamo_3
            ColumnLayout {
                Text {
                    text: "Plasma Mobile"
                    color: "white"
                    Layout.alignment: Qt.AlignCenter
                    font.pointSize: 40
                }
                Text {
                    Layout.alignment: Qt.AlignCenter
                    text: "🏃🏽 Fits in your pocket"
                    color: "white"
                    font.pointSize: 30
                }
                Text {
                    Layout.alignment: Qt.AlignCenter
                    text: "📱 Plasma for mobile devices"
                    color: "white"
                    font.pointSize: 30
                }
                Text {
                    Layout.alignment: Qt.AlignCenter
                    text: "🖐️ UI optimized for touch"
                    color: "white"
                    font.pointSize: 30
                }
                Text {
                    Layout.alignment: Qt.AlignCenter
                    text: "👩🏼‍🏫 100% Free and Open Source phone"
                    color: "white"
                    font.pointSize: 30
                }
            }
        }
        Component {
            id: plamo_4
            ColumnLayout {
                OurImage {
                    source: "plasmaphone.svg"
                }
                Text {
                    text: "https://www.plasma-mobile.org/"
                    color: "white"
                    Layout.alignment: Qt.AlignCenter
                    font.pointSize: 40
                }
            }
        }
        Component {
            id: kirigami_1
            ColumnLayout {
                OurImage {
                    source: "kirigami-adapt.png"
                }
                Text {
                    text: "Kirigami"
                    color: "white"
                    Layout.alignment: Qt.AlignCenter
                    font.pointSize: 40
                }
            }
        }
        Component {
            id: kirigami_2
            ColumnLayout {
                OurImage {
                    source: "kirigami-adapt.png"
                }
                Text {
                    text: "https://kde.org/products/kirigami/"
                    color: "white"
                    Layout.alignment: Qt.AlignCenter
                    font.pointSize: 40
                }
            }
        }
        Component {
            id: kirigami_3
            ColumnLayout {
                Text {
                    text: "Kirigami"
                    color: "white"
                    Layout.alignment: Qt.AlignCenter
                    font.pointSize: 40
                }
                Text {
                    Layout.alignment: Qt.AlignCenter
                    text: "🏃🏽 Fits in your pocket and your computer 🖥️"
                    color: "white"
                    font.pointSize: 30
                }
                Text {
                    Layout.alignment: Qt.AlignCenter
                    text: "🖐️ UI switches for touch and mouse 🖱️"
                    color: "white"
                    font.pointSize: 30
                }
                Text {
                    Layout.alignment: Qt.AlignCenter
                    text: "🔧 Developer friendly"
                    color: "white"
                    font.pointSize: 30
                }
                Text {
                    Layout.alignment: Qt.AlignCenter
                    text: "👩🏼‍🏫 100% Free and Open Source"
                    color: "white"
                    font.pointSize: 30
                }
            }
        }
        Component {
            id: kirigami_4
            ColumnLayout {
                OurImage {
                    source: "kirigami-adapt.png"
                }
                Text {
                    text: "https://kde.org/products/kirigami/"
                    color: "white"
                    Layout.alignment: Qt.AlignCenter
                    font.pointSize: 40
                }
            }
        }
        Component {
            id: visionKDE
            ColumnLayout {
                Kirigami.Icon {
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    source: "kde"
                }
                Text {
                    Layout.alignment: Qt.AlignCenter
                    horizontalAlignment: Text.AlignCenter
                    text: "“A world in which everyone has control over \ntheir digital life and enjoys freedom and privacy.”"
                    color: "white"
                    font.pointSize: 30
                    font.italic: true
                }
            }
        }
        Component {
            id: visionPlasma
            ColumnLayout {
                Kirigami.Icon {
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    source: "plasma"
                }
                Text {
                    Layout.alignment: Qt.AlignCenter
                    horizontalAlignment: Text.AlignCenter
                    text: "“Simple by default\nPowerful when needed.”"
                    color: "white"
                    font.pointSize: 30
                    font.italic: true
                }
            }
        }

        Timer {
            interval: 3500
            running: true
            repeat: true
            onTriggered: {
                parent.next()
            }
        }

        focus: true
        function next() { current = (current + 1) % comps.length }
        Keys.onRightPressed: next()

        property int current: 0
        property var comps: [ visionPlasma, visionKDE, plamo_1, plamo_2, plamo_3, plamo_4, kirigami_1, kirigami_2, kirigami_3, kirigami_4 ]
        sourceComponent: comps[current]
    }
}
